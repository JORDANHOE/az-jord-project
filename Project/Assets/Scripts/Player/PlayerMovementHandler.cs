﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementHandler : MonoBehaviour
{
	private CharacterController controller;
	private Vector3 movementDirection;

	public float movementSpeed;
	public float midairModifier;
	public float jumpHeight;
	public float gravity;

	private void Start() {

		controller = this.GetComponent<CharacterController>();
	}

	private void Update() {

		float x = Input.GetAxis("Horizontal");
		float z = Input.GetAxis("Vertical");

		if (controller.isGrounded) {

			movementDirection = new Vector3(x, 0, z);
			movementDirection = transform.TransformDirection(movementDirection);
			movementDirection *= movementSpeed;

			if (Input.GetButtonDown("Jump")) {

				movementDirection.y = jumpHeight;
			}
		} else {

			movementDirection.x = x * movementSpeed;
			movementDirection.z = z * movementSpeed;
			movementDirection = transform.TransformDirection(movementDirection);
		}

		movementDirection.y -= gravity * Time.deltaTime;

		controller.Move(movementDirection * Time.deltaTime);
	}
}
